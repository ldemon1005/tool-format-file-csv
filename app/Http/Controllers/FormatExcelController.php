<?php

namespace App\Http\Controllers;

use App\Jobs\FormatFileExcel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

class FormatExcelController extends Controller
{
    public function uploadFile(Request $request){
        $file = $request->file('file');
        if($file->getClientOriginalExtension() != 'csv'){
            return back()->with('error','File type is not csv');
        }

        $title = [];
        $file_name = date('d-m-Y-H-i', time()) . '-' . $file->getClientOriginalName();
        if(Storage::disk('public_excel')->put($file_name, file_get_contents($file))) {
            $in = fopen(public_path('input excel/' . $file_name), 'r');
            while (!feof($in)) {
                $title = fgetcsv($in);break;
            }
        }else {
            back()->with('error','Upload file error.');
        }
        $data = compact('title','file_name');
        return view('index',$data);
    }

    public function formatExcel(Request $request){
        $conditions = $request->all();
        $file_name = $conditions['file_name'];
        unset($conditions['file_name']);
        unset($conditions['_token']);
        $path = public_path('input excel/' . $file_name);
        $splitFile = $this->splitFile($path);
        $listFile = $splitFile['listFile'];
        $folder = date('d-m-Y-H-i');
        foreach ($listFile as $key => $file){
            if($key < count($listFile) - 2) {
                if($key != count($listFile) - 3){
                    $format = (new FormatFileExcel($folder, $file, $key + 1, $conditions, false, $splitFile['tempPath']))->delay(Carbon::now()->addSeconds(3));
                    $this->dispatch($format);
                }else {
                    $format = (new FormatFileExcel($folder, $file, $key + 1, $conditions, true, $splitFile['tempPath']))->delay(Carbon::now()->addSeconds(3));
                    $this->dispatch($format);
                }
            }
        }
        echo 'oke rồi';
    }

    function splitFile($inputFile = ''){
        $path = public_path('temp_excel/' . date('d-m-Y-H-i') . '/');

        if(!file_exists($path)){
            mkdir($path);
        }

        $outputFiles = [];

        $outputFile = 'output';

        $splitSize = 1000;

        $in = fopen($inputFile, 'r');

        $rowCount = 0;
        $fileCount = 1;
        $title = '';
        while (!feof($in)) {

            if (($rowCount % $splitSize) == 0) {
                if ($rowCount > 0) {
                    fclose($out);
                }
                $out = fopen($path . $outputFile . $fileCount++ . '.csv', 'w');
                array_push($outputFiles,$path . $outputFile . $fileCount . '.csv');
            }
            $data = fgetcsv($in);
            if($rowCount == 0) $title = $data;
            if($rowCount > 0 && $rowCount % $splitSize == 0) fputcsv($out, $title);
            if ($data)
                fputcsv($out, $data);
            $rowCount++;
        }
        fclose($out);

        return [
            'tempPath' => $path,
            'listFile' => $outputFiles
        ];
    }

}
