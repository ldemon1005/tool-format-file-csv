<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class FormatFileExcel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $folder;
    private $file_path;
    private $queue_order;
    private $conditions;
    private $flag_del;
    private $tempPath;

    private $type = [
        'integer' => 1,
        'double'  => 2,
        'text'    => 3,
        'date'    => 4,
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($folder = '', $file_path = '',$queue_order = 1, $conditions, $flag_del = false, $tempPath)
    {
        $this->folder = $folder;
        $this->file_path = $file_path;
        $this->queue_order = $queue_order;
        $this->conditions = $conditions;
        $this->flag_del = $flag_del;
        $this->tempPath = $tempPath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $format = 'Y-m-d H:i:s';

        $data = Excel::load($this->file_path)->toArray();
        if(count($data)){
            foreach ($data as $row){
                foreach ($this->conditions as $key => $condition){
                    $key = strtolower($key);

                    switch ($condition['type']){
                        case $this->type['integer']:
                            if(isset($row[$key]) && is_integer($row[$key]) ){
                                $row[$key] = $condition['default'];
                            }
                            break;
                        case $this->type['double']:
                            if(isset($row[$key]) && is_double($row[$key]) ){
                                $row[$key] = $condition['default'];
                            }
                            break;
                        case $this->type['text']:
                            if(isset($row[$key]) && (is_null($row[$key]) || $row[$key] == '')){
                                $row[$key] = $condition['default'];
                            }
                            break;
                        case $this->type['date']:
                            if(isset($row[$key])){
                                $d = \DateTime::createFromFormat($format, $row[$key]);
                                if(!($d && $d->format($format) == $row[$key])){
                                    $row[$key] = $condition['default'];
                                }
                            }
                            break;
                    }
                }
            }
        }
        $path = public_path().'/outputfile/'. $this->folder;

        if(!file_exists($path)){
            mkdir($path);
        }
        Excel::create('file'.$this->queue_order, function($excel) use($data) {
            $excel->sheet('Sheet1', function($sheet) use ($data){
                $sheet->fromArray($data);
            });
        })->store('csv',$path);

        if($this->flag_del == true){
            File::deleteDirectory($this->tempPath);
        }
    }
}
