@extends('master')
@section('main')
    <div class="row mt-5">
        <form class="w-100" action="{{route('upload_excel')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="input-group">
                <div class="input-group-prepend">
                    <button class="input-group-text btn btn-primary" type="submit" id="inputGroupFileAddon01">Upload</button>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" required
                           aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                </div>
            </div>
        </form>
    </div>

    @if(isset($title))
        <div class="row mt-5">
            <form class="w-100" action="{{route('format_excel')}}" method="post">
                {{csrf_field()}}
                <input type="text" name="file_name" value="{{$file_name}}" hidden>
                <div class="col-12 mb-3">
                    <button type="submit" class="btn btn-primary">FORMAT FILE</button>
                </div>
                @foreach($title as $item)
                    <div class="col-12">
                        <div class="row form-group">
                            <label class="col-3">{{$item}}</label>
                            <div class="col-3">
                                <select name="{{$item}}[type]" class="form-control">
                                    <option value="0">Type</option>
                                    <option value="1">Integer</option>
                                    <option value="2">Double</option>
                                    <option value="3">Text</option>
                                    <option value="4">Date</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">value default</span>
                                    </div>
                                    <input type="text" class="form-control" name="{{$item}}[default]" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-12">
                    <button type="submit" class="btn btn-primary float-right">FORMAT FILE</button>
                </div>
            </form>
        </div>
    @endif
@stop
